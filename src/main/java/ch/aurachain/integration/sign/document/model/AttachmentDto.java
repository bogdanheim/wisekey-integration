package ch.aurachain.integration.sign.document.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class AttachmentDto {

    private String id;
    private String fileName;
    private String contentType;
    private Long size;
    private LocalDateTime modifiedOn;
    private String userId;
    private String activityId;
    private String userName;
    private String activityType;
    private String activityName;
    private String dataModelPath;
    private String pathOnDisk;
    private Boolean isGenerated;

}
