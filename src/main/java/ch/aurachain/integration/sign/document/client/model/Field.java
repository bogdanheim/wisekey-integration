/*
 * Api Documentation
 * Api Documentation
 *
 * OpenAPI spec version: 1.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ch.aurachain.integration.sign.document.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * Field
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-10T06:54:18.464Z")
public class Field {
  @SerializedName("id")
  private String id = null;

  @SerializedName("isBCReady")
  private Boolean isBCReady = null;

  @SerializedName("isCat")
  private Boolean isCat = null;

  @SerializedName("isEmbeded")
  private Boolean isEmbeded = null;

  @SerializedName("isIndexable")
  private Boolean isIndexable = null;

  @SerializedName("isList")
  private Boolean isList = null;

  @SerializedName("name")
  private String name = null;

  @SerializedName("notificationOnly")
  private Boolean notificationOnly = null;

  @SerializedName("type")
  private String type = null;

  public Field id(String id) {
    this.id = id;
    return this;
  }

   /**
   * Get id
   * @return id
  **/
  @ApiModelProperty(value = "")
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Field isBCReady(Boolean isBCReady) {
    this.isBCReady = isBCReady;
    return this;
  }

   /**
   * Get isBCReady
   * @return isBCReady
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsBCReady() {
    return isBCReady;
  }

  public void setIsBCReady(Boolean isBCReady) {
    this.isBCReady = isBCReady;
  }

  public Field isCat(Boolean isCat) {
    this.isCat = isCat;
    return this;
  }

   /**
   * Get isCat
   * @return isCat
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsCat() {
    return isCat;
  }

  public void setIsCat(Boolean isCat) {
    this.isCat = isCat;
  }

  public Field isEmbeded(Boolean isEmbeded) {
    this.isEmbeded = isEmbeded;
    return this;
  }

   /**
   * Get isEmbeded
   * @return isEmbeded
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsEmbeded() {
    return isEmbeded;
  }

  public void setIsEmbeded(Boolean isEmbeded) {
    this.isEmbeded = isEmbeded;
  }

  public Field isIndexable(Boolean isIndexable) {
    this.isIndexable = isIndexable;
    return this;
  }

   /**
   * Get isIndexable
   * @return isIndexable
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsIndexable() {
    return isIndexable;
  }

  public void setIsIndexable(Boolean isIndexable) {
    this.isIndexable = isIndexable;
  }

  public Field isList(Boolean isList) {
    this.isList = isList;
    return this;
  }

   /**
   * Get isList
   * @return isList
  **/
  @ApiModelProperty(value = "")
  public Boolean isIsList() {
    return isList;
  }

  public void setIsList(Boolean isList) {
    this.isList = isList;
  }

  public Field name(String name) {
    this.name = name;
    return this;
  }

   /**
   * Get name
   * @return name
  **/
  @ApiModelProperty(value = "")
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Field notificationOnly(Boolean notificationOnly) {
    this.notificationOnly = notificationOnly;
    return this;
  }

   /**
   * Get notificationOnly
   * @return notificationOnly
  **/
  @ApiModelProperty(value = "")
  public Boolean isNotificationOnly() {
    return notificationOnly;
  }

  public void setNotificationOnly(Boolean notificationOnly) {
    this.notificationOnly = notificationOnly;
  }

  public Field type(String type) {
    this.type = type;
    return this;
  }

   /**
   * Get type
   * @return type
  **/
  @ApiModelProperty(value = "")
  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    Field field = (Field) o;
    return Objects.equals(this.id, field.id) &&
        Objects.equals(this.isBCReady, field.isBCReady) &&
        Objects.equals(this.isCat, field.isCat) &&
        Objects.equals(this.isEmbeded, field.isEmbeded) &&
        Objects.equals(this.isIndexable, field.isIndexable) &&
        Objects.equals(this.isList, field.isList) &&
        Objects.equals(this.name, field.name) &&
        Objects.equals(this.notificationOnly, field.notificationOnly) &&
        Objects.equals(this.type, field.type);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, isBCReady, isCat, isEmbeded, isIndexable, isList, name, notificationOnly, type);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class Field {\n");
    
    sb.append("    id: ").append(toIndentedString(id)).append("\n");
    sb.append("    isBCReady: ").append(toIndentedString(isBCReady)).append("\n");
    sb.append("    isCat: ").append(toIndentedString(isCat)).append("\n");
    sb.append("    isEmbeded: ").append(toIndentedString(isEmbeded)).append("\n");
    sb.append("    isIndexable: ").append(toIndentedString(isIndexable)).append("\n");
    sb.append("    isList: ").append(toIndentedString(isList)).append("\n");
    sb.append("    name: ").append(toIndentedString(name)).append("\n");
    sb.append("    notificationOnly: ").append(toIndentedString(notificationOnly)).append("\n");
    sb.append("    type: ").append(toIndentedString(type)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

