package ch.aurachain.integration.sign.jsondb.model;

import io.jsondb.annotation.Document;
import io.jsondb.annotation.Id;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Document(collection = "signDocuments", schemaVersion = "1.0")
public class SignDocument {

    @Id
    private String id;

    private String fileUUID;
    private String fileName;
    private String processInstID;

    private String status;

    private String requester;
    private String signer;

    private String signingCode;
    private String expireTime;
    private Long timestamp;

    private String uploadActivity;
    private String uploadKey;
}
