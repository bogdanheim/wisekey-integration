/*
 * Signature Service APIs
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ch.aurachain.integration.sign.wisekey.client.api;

import ch.aurachain.integration.sign.wisekey.client.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TimestampApi {
    private ApiClient apiClient;

    public TimestampApi() {
        this(Configuration.getDefaultApiClient());
    }

    public TimestampApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for timestampGet
     * @param message content of message (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call timestampGetCall(String message, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/tsp/{message}"
            .replaceAll("\\{" + "message" + "\\}", apiClient.escapeString(message.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (X_APP_ID != null)
        localVarHeaderParams.put("X-APP-ID", apiClient.parameterToString(X_APP_ID));
        if (xDate != null)
        localVarHeaderParams.put("X-Date", apiClient.parameterToString(xDate));
        if (X_UID != null)
        localVarHeaderParams.put("X-UID", apiClient.parameterToString(X_UID));
        if (xSignature != null)
        localVarHeaderParams.put("X-Signature", apiClient.parameterToString(xSignature));
        if (xVersion != null)
        localVarHeaderParams.put("X-Version", apiClient.parameterToString(xVersion));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "text/json", "text/html"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call timestampGetValidateBeforeCall(String message, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'message' is set
        if (message == null) {
            throw new ApiException("Missing the required parameter 'message' when calling timestampGet(Async)");
        }
        

        com.squareup.okhttp.Call call = timestampGetCall(message, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Timestamp server with message (maybe user for Adobe DC)
     * 
     * @param message content of message (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public void timestampGet(String message, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        timestampGetWithHttpInfo(message, X_APP_ID, xDate, X_UID, xSignature, xVersion);
    }

    /**
     * Timestamp server with message (maybe user for Adobe DC)
     * 
     * @param message content of message (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Void> timestampGetWithHttpInfo(String message, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        com.squareup.okhttp.Call call = timestampGetValidateBeforeCall(message, X_APP_ID, xDate, X_UID, xSignature, xVersion, null, null);
        return apiClient.execute(call);
    }

    /**
     * Timestamp server with message (maybe user for Adobe DC) (asynchronously)
     * 
     * @param message content of message (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call timestampGetAsync(String message, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = timestampGetValidateBeforeCall(message, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
    /**
     * Build call for timestampPost
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call timestampPostCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/tsp";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "text/json", "text/html"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call timestampPostValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        

        com.squareup.okhttp.Call call = timestampPostCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Timestamp server (maybe user for Adobe DC)
     * 
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public void timestampPost() throws ApiException {
        timestampPostWithHttpInfo();
    }

    /**
     * Timestamp server (maybe user for Adobe DC)
     * 
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Void> timestampPostWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = timestampPostValidateBeforeCall(null, null);
        return apiClient.execute(call);
    }

    /**
     * Timestamp server (maybe user for Adobe DC) (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call timestampPostAsync(final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = timestampPostValidateBeforeCall(progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }
}
