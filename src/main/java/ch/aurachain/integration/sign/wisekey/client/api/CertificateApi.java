/*
 * Signature Service APIs
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ch.aurachain.integration.sign.wisekey.client.api;

import ch.aurachain.integration.sign.wisekey.client.*;
import ch.aurachain.integration.sign.wisekey.client.model.*;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CertificateApi {
    private ApiClient apiClient;

    public CertificateApi() {
        this(Configuration.getDefaultApiClient());
    }

    public CertificateApi(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    public void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for certificateGet
     * @param cidKeyOwner Email of the owner.               The signing certificate of the requested signer must be generated WISeKey CIDCMS (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ch.aurachain.integration.sign.wisekey.client.ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call certificateGetCall(String cidKeyOwner, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/certificate/{cidKeyOwner}"
            .replaceAll("\\{" + "cidKeyOwner" + "\\}", apiClient.escapeString(cidKeyOwner.toString()));

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (X_APP_ID != null)
        localVarHeaderParams.put("X-APP-ID", apiClient.parameterToString(X_APP_ID));
        if (xDate != null)
        localVarHeaderParams.put("X-Date", apiClient.parameterToString(xDate));
        if (X_UID != null)
        localVarHeaderParams.put("X-UID", apiClient.parameterToString(X_UID));
        if (xSignature != null)
        localVarHeaderParams.put("X-Signature", apiClient.parameterToString(xSignature));
        if (xVersion != null)
        localVarHeaderParams.put("X-Version", apiClient.parameterToString(xVersion));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "text/json", "text/html"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call certificateGetValidateBeforeCall(String cidKeyOwner, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'cidKeyOwner' is set
        if (cidKeyOwner == null) {
            throw new ApiException("Missing the required parameter 'cidKeyOwner' when calling certificateGet(Async)");
        }
        

        com.squareup.okhttp.Call call = certificateGetCall(cidKeyOwner, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Get certificates of a given owner of the certificates.
     * 
     * @param cidKeyOwner Email of the owner.               The signing certificate of the requested signer must be generated WISeKey CIDCMS (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return SigningCertificateResult
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SigningCertificateResult certificateGet(String cidKeyOwner, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        ApiResponse<SigningCertificateResult> resp = certificateGetWithHttpInfo(cidKeyOwner, X_APP_ID, xDate, X_UID, xSignature, xVersion);
        return resp.getData();
    }

    /**
     * Get certificates of a given owner of the certificates.
     * 
     * @param cidKeyOwner Email of the owner.               The signing certificate of the requested signer must be generated WISeKey CIDCMS (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return ApiResponse&lt;SigningCertificateResult&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SigningCertificateResult> certificateGetWithHttpInfo(String cidKeyOwner, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        com.squareup.okhttp.Call call = certificateGetValidateBeforeCall(cidKeyOwner, X_APP_ID, xDate, X_UID, xSignature, xVersion, null, null);
        Type localVarReturnType = new TypeToken<SigningCertificateResult>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Get certificates of a given owner of the certificates. (asynchronously)
     * 
     * @param cidKeyOwner Email of the owner.               The signing certificate of the requested signer must be generated WISeKey CIDCMS (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call certificateGetAsync(String cidKeyOwner, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ApiCallback<SigningCertificateResult> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = certificateGetValidateBeforeCall(cidKeyOwner, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SigningCertificateResult>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for certificateImportKey
     * @param keyData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call certificateImportKeyCall(KeyImportRequest keyData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = keyData;

        // create path and map variables
        String localVarPath = "/certificate/import";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (X_APP_ID != null)
        localVarHeaderParams.put("X-APP-ID", apiClient.parameterToString(X_APP_ID));
        if (xDate != null)
        localVarHeaderParams.put("X-Date", apiClient.parameterToString(xDate));
        if (X_UID != null)
        localVarHeaderParams.put("X-UID", apiClient.parameterToString(X_UID));
        if (xSignature != null)
        localVarHeaderParams.put("X-Signature", apiClient.parameterToString(xSignature));
        if (xVersion != null)
        localVarHeaderParams.put("X-Version", apiClient.parameterToString(xVersion));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "text/json", "text/html"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json", "text/json", "text/html", "application/x-www-form-urlencoded"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call certificateImportKeyValidateBeforeCall(KeyImportRequest keyData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'keyData' is set
        if (keyData == null) {
            throw new ApiException("Missing the required parameter 'keyData' when calling certificateImportKey(Async)");
        }
        

        com.squareup.okhttp.Call call = certificateImportKeyCall(keyData, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Request to import a PKCS12 certificate in HSM
     * 
     * @param keyData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return Object
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Object certificateImportKey(KeyImportRequest keyData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        ApiResponse<Object> resp = certificateImportKeyWithHttpInfo(keyData, X_APP_ID, xDate, X_UID, xSignature, xVersion);
        return resp.getData();
    }

    /**
     * Request to import a PKCS12 certificate in HSM
     * 
     * @param keyData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return ApiResponse&lt;Object&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Object> certificateImportKeyWithHttpInfo(KeyImportRequest keyData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        com.squareup.okhttp.Call call = certificateImportKeyValidateBeforeCall(keyData, X_APP_ID, xDate, X_UID, xSignature, xVersion, null, null);
        Type localVarReturnType = new TypeToken<Object>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Request to import a PKCS12 certificate in HSM (asynchronously)
     * 
     * @param keyData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call certificateImportKeyAsync(KeyImportRequest keyData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ApiCallback<Object> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = certificateImportKeyValidateBeforeCall(keyData, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Object>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for certificateRemoveCert
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call certificateRemoveCertCall(CertificateRemoveRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = certData;

        // create path and map variables
        String localVarPath = "/certificate/remove";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (X_APP_ID != null)
        localVarHeaderParams.put("X-APP-ID", apiClient.parameterToString(X_APP_ID));
        if (xDate != null)
        localVarHeaderParams.put("X-Date", apiClient.parameterToString(xDate));
        if (X_UID != null)
        localVarHeaderParams.put("X-UID", apiClient.parameterToString(X_UID));
        if (xSignature != null)
        localVarHeaderParams.put("X-Signature", apiClient.parameterToString(xSignature));
        if (xVersion != null)
        localVarHeaderParams.put("X-Version", apiClient.parameterToString(xVersion));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "text/json", "text/html"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json", "text/json", "text/html", "application/x-www-form-urlencoded"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call certificateRemoveCertValidateBeforeCall(CertificateRemoveRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'certData' is set
        if (certData == null) {
            throw new ApiException("Missing the required parameter 'certData' when calling certificateRemoveCert(Async)");
        }
        

        com.squareup.okhttp.Call call = certificateRemoveCertCall(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Request to remove a PKCS12 certificate in HSM
     * 
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return CertificateRemoveResult
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CertificateRemoveResult certificateRemoveCert(CertificateRemoveRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        ApiResponse<CertificateRemoveResult> resp = certificateRemoveCertWithHttpInfo(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion);
        return resp.getData();
    }

    /**
     * Request to remove a PKCS12 certificate in HSM
     * 
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return ApiResponse&lt;CertificateRemoveResult&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CertificateRemoveResult> certificateRemoveCertWithHttpInfo(CertificateRemoveRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        com.squareup.okhttp.Call call = certificateRemoveCertValidateBeforeCall(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion, null, null);
        Type localVarReturnType = new TypeToken<CertificateRemoveResult>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Request to remove a PKCS12 certificate in HSM (asynchronously)
     * 
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call certificateRemoveCertAsync(CertificateRemoveRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ApiCallback<CertificateRemoveResult> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = certificateRemoveCertValidateBeforeCall(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CertificateRemoveResult>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for certificateValidateCert
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call certificateValidateCertCall(CertificateValidateRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = certData;

        // create path and map variables
        String localVarPath = "/certificate/validate";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();
        List<Pair> localVarCollectionQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();
        if (X_APP_ID != null)
        localVarHeaderParams.put("X-APP-ID", apiClient.parameterToString(X_APP_ID));
        if (xDate != null)
        localVarHeaderParams.put("X-Date", apiClient.parameterToString(xDate));
        if (X_UID != null)
        localVarHeaderParams.put("X-UID", apiClient.parameterToString(X_UID));
        if (xSignature != null)
        localVarHeaderParams.put("X-Signature", apiClient.parameterToString(xSignature));
        if (xVersion != null)
        localVarHeaderParams.put("X-Version", apiClient.parameterToString(xVersion));

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
            "application/json", "text/json", "text/html"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json", "text/json", "text/html", "application/x-www-form-urlencoded"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                    .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                    .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] {  };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call certificateValidateCertValidateBeforeCall(CertificateValidateRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'certData' is set
        if (certData == null) {
            throw new ApiException("Missing the required parameter 'certData' when calling certificateValidateCert(Async)");
        }
        

        com.squareup.okhttp.Call call = certificateValidateCertCall(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Request to validate a signing certificate
     * 
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return CertificateValidateResult
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public CertificateValidateResult certificateValidateCert(CertificateValidateRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        ApiResponse<CertificateValidateResult> resp = certificateValidateCertWithHttpInfo(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion);
        return resp.getData();
    }

    /**
     * Request to validate a signing certificate
     * 
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @return ApiResponse&lt;CertificateValidateResult&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<CertificateValidateResult> certificateValidateCertWithHttpInfo(CertificateValidateRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion) throws ApiException {
        com.squareup.okhttp.Call call = certificateValidateCertValidateBeforeCall(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion, null, null);
        Type localVarReturnType = new TypeToken<CertificateValidateResult>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Request to validate a signing certificate (asynchronously)
     * 
     * @param certData  (required)
     * @param X_APP_ID Application ID (optional)
     * @param xDate Request timestamp (optional)
     * @param X_UID Encrypted User Identifier (optional)
     * @param xSignature Signature signed by the application key (optional)
     * @param xVersion API Version: it can be 1.0 or 1.1 (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call certificateValidateCertAsync(CertificateValidateRequest certData, String X_APP_ID, String xDate, String X_UID, String xSignature, String xVersion, final ApiCallback<CertificateValidateResult> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = certificateValidateCertValidateBeforeCall(certData, X_APP_ID, xDate, X_UID, xSignature, xVersion, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<CertificateValidateResult>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
