/*
 * Signature Service APIs
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ch.aurachain.integration.sign.wisekey.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;
import org.threeten.bp.OffsetDateTime;

/**
 * SigningCertificateDetailResult
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-09T13:05:56.469Z")
public class SigningCertificateDetailResult {
  @SerializedName("serialNumber")
  private String serialNumber = null;

  @SerializedName("thumbPrint")
  private String thumbPrint = null;

  @SerializedName("certificate")
  private String certificate = null;

  @SerializedName("keyID")
  private String keyID = null;

  @SerializedName("status")
  private String status = null;

  @SerializedName("expiredDate")
  private OffsetDateTime expiredDate = null;

  public SigningCertificateDetailResult serialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
    return this;
  }

   /**
   * Get serialNumber
   * @return serialNumber
  **/
  @ApiModelProperty(value = "")
  public String getSerialNumber() {
    return serialNumber;
  }

  public void setSerialNumber(String serialNumber) {
    this.serialNumber = serialNumber;
  }

  public SigningCertificateDetailResult thumbPrint(String thumbPrint) {
    this.thumbPrint = thumbPrint;
    return this;
  }

   /**
   * Get thumbPrint
   * @return thumbPrint
  **/
  @ApiModelProperty(value = "")
  public String getThumbPrint() {
    return thumbPrint;
  }

  public void setThumbPrint(String thumbPrint) {
    this.thumbPrint = thumbPrint;
  }

  public SigningCertificateDetailResult certificate(String certificate) {
    this.certificate = certificate;
    return this;
  }

   /**
   * Get certificate
   * @return certificate
  **/
  @ApiModelProperty(value = "")
  public String getCertificate() {
    return certificate;
  }

  public void setCertificate(String certificate) {
    this.certificate = certificate;
  }

  public SigningCertificateDetailResult keyID(String keyID) {
    this.keyID = keyID;
    return this;
  }

   /**
   * Get keyID
   * @return keyID
  **/
  @ApiModelProperty(value = "")
  public String getKeyID() {
    return keyID;
  }

  public void setKeyID(String keyID) {
    this.keyID = keyID;
  }

  public SigningCertificateDetailResult status(String status) {
    this.status = status;
    return this;
  }

   /**
   * Get status
   * @return status
  **/
  @ApiModelProperty(value = "")
  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public SigningCertificateDetailResult expiredDate(OffsetDateTime expiredDate) {
    this.expiredDate = expiredDate;
    return this;
  }

   /**
   * Get expiredDate
   * @return expiredDate
  **/
  @ApiModelProperty(value = "")
  public OffsetDateTime getExpiredDate() {
    return expiredDate;
  }

  public void setExpiredDate(OffsetDateTime expiredDate) {
    this.expiredDate = expiredDate;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    SigningCertificateDetailResult signingCertificateDetailResult = (SigningCertificateDetailResult) o;
    return Objects.equals(this.serialNumber, signingCertificateDetailResult.serialNumber) &&
        Objects.equals(this.thumbPrint, signingCertificateDetailResult.thumbPrint) &&
        Objects.equals(this.certificate, signingCertificateDetailResult.certificate) &&
        Objects.equals(this.keyID, signingCertificateDetailResult.keyID) &&
        Objects.equals(this.status, signingCertificateDetailResult.status) &&
        Objects.equals(this.expiredDate, signingCertificateDetailResult.expiredDate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(serialNumber, thumbPrint, certificate, keyID, status, expiredDate);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class SigningCertificateDetailResult {\n");
    
    sb.append("    serialNumber: ").append(toIndentedString(serialNumber)).append("\n");
    sb.append("    thumbPrint: ").append(toIndentedString(thumbPrint)).append("\n");
    sb.append("    certificate: ").append(toIndentedString(certificate)).append("\n");
    sb.append("    keyID: ").append(toIndentedString(keyID)).append("\n");
    sb.append("    status: ").append(toIndentedString(status)).append("\n");
    sb.append("    expiredDate: ").append(toIndentedString(expiredDate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

