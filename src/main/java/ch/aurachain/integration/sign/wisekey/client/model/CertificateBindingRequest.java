/*
 * Signature Service APIs
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package ch.aurachain.integration.sign.wisekey.client.model;

import java.util.Objects;
import java.util.Arrays;
import com.google.gson.TypeAdapter;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.IOException;

/**
 * CertificateBindingRequest
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.JavaClientCodegen", date = "2020-01-09T13:05:56.469Z")
public class CertificateBindingRequest {
  @SerializedName("cidKeyOwner")
  private String cidKeyOwner = null;

  @SerializedName("keyID")
  private String keyID = null;

  @SerializedName("certificate")
  private String certificate = null;

  public CertificateBindingRequest cidKeyOwner(String cidKeyOwner) {
    this.cidKeyOwner = cidKeyOwner;
    return this;
  }

   /**
   * Get cidKeyOwner
   * @return cidKeyOwner
  **/
  @ApiModelProperty(value = "")
  public String getCidKeyOwner() {
    return cidKeyOwner;
  }

  public void setCidKeyOwner(String cidKeyOwner) {
    this.cidKeyOwner = cidKeyOwner;
  }

  public CertificateBindingRequest keyID(String keyID) {
    this.keyID = keyID;
    return this;
  }

   /**
   * Get keyID
   * @return keyID
  **/
  @ApiModelProperty(value = "")
  public String getKeyID() {
    return keyID;
  }

  public void setKeyID(String keyID) {
    this.keyID = keyID;
  }

  public CertificateBindingRequest certificate(String certificate) {
    this.certificate = certificate;
    return this;
  }

   /**
   * Get certificate
   * @return certificate
  **/
  @ApiModelProperty(value = "")
  public String getCertificate() {
    return certificate;
  }

  public void setCertificate(String certificate) {
    this.certificate = certificate;
  }


  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    CertificateBindingRequest certificateBindingRequest = (CertificateBindingRequest) o;
    return Objects.equals(this.cidKeyOwner, certificateBindingRequest.cidKeyOwner) &&
        Objects.equals(this.keyID, certificateBindingRequest.keyID) &&
        Objects.equals(this.certificate, certificateBindingRequest.certificate);
  }

  @Override
  public int hashCode() {
    return Objects.hash(cidKeyOwner, keyID, certificate);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class CertificateBindingRequest {\n");
    
    sb.append("    cidKeyOwner: ").append(toIndentedString(cidKeyOwner)).append("\n");
    sb.append("    keyID: ").append(toIndentedString(keyID)).append("\n");
    sb.append("    certificate: ").append(toIndentedString(certificate)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

