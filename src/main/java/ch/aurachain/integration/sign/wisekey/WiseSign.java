package ch.aurachain.integration.sign.wisekey;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.json.JSONObject;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.*;
import java.text.SimpleDateFormat;
import java.util.*;

public class WiseSign {
	private static final char[] HEX_CHARS = "0123456789abcdef".toCharArray();
	private static final String CHARACTER_ENCODING = "UTF-8";
	private static final String CIPHER_TRANSFORMATION = "AES/CBC/PKCS7Padding";

	private static final String AES_ALGORITHM = "AES";
	private static final int IV_SIZE = 16;
	private static final int AES256_KEY_SIZE = 32;
	// APIs
	private static final String HEADER_CONTENT_TYPE = "Content-Type";
	private static final String HEADER_X_DATE = "X-Date";
	private static final String HEADER_X_SIGNATURE = "X-Signature";
	private static final String HEADER_X_UID = "X-UID";
	private static final String HEADER_X_VERSION = "X-Version";
	private static final String HEADER_X_APP_ID = "X-APP-ID";

	private static final String JSON_CONTENT_TYPE = "application/json";

	private static final String BASE_DOC_URL = "https://cloudsign.wiseid.com/api";
	private static final String SS_APP_ID = "add97fe9-1d38-4059-b461-638a68351c6a";
	private static final String SS_API_SECRET_KEY = "1ff174f3-1908-4bb0-ac1c-71c30710523e";
	private static final String SS_API_VERSION = "1.1";

	// End points
	private static final String PENDING_DOCUMENTS = "/signingrequest";
	private static final String DEVICE_REGISTER = "/device/register";
	private static final String SIGN_PDF = "/key/signpdf";
	private static final String GET_SIGNED_DOC = "/doc";

	public static void main(String[] args) {
		try {
			Security.addProvider(new BouncyCastleProvider());
			// Test
//			String result = signPdf("bogdan.heim@aurachain.ch");
//			System.out.println(result);
		} catch (Throwable t) {
			t.printStackTrace();
		}
	}

	// APIs
	public static String getSignedDocument(final String email, final String signingCode) throws Exception {
		final String uid = getSignUid(email, SS_API_SECRET_KEY);
		final String date = getCurrentDateTimeGMT();
		final String signature = createSignatureHasUid(SS_API_SECRET_KEY, uid, "GET", "", JSON_CONTENT_TYPE, date, "",
				GET_SIGNED_DOC, null);
		// Headers
		HashMap<String, String> headerMap = new HashMap<>();
		headerMap.put(HEADER_CONTENT_TYPE, JSON_CONTENT_TYPE);
		headerMap.put(HEADER_X_DATE, date);
		headerMap.put(HEADER_X_APP_ID, SS_APP_ID);
		headerMap.put(HEADER_X_UID, uid);
		headerMap.put(HEADER_X_SIGNATURE, signature);
		headerMap.put(HEADER_X_VERSION, SS_API_VERSION);
		final String urlText = BASE_DOC_URL.concat(GET_SIGNED_DOC).concat("/").concat(signingCode);
		return getData(urlText, headerMap);
	}

	public static String signPdf(final String user, final String email, final String docUri, final String callBackUri, final Map<String, Object> metaData, final Integer x, final Integer y, final Integer page) throws Exception {
		final String uid = getSignUid(email, SS_API_SECRET_KEY);
		final String date = getCurrentDateTimeGMT();
		// Body
		JSONObject doc = new JSONObject();
		doc.put("docUri", docUri); //"http://www.pdf995.com/samples/pdf.pdf");
		doc.put("signatureOrder", -1);
		doc.put("hashAlgo", "SHA256");
		doc.put("requester", user);//"bogdan.heim@aurachain.ch");
		doc.put("cidKeyOwner", email);
		doc.put("approvals", 1);
		doc.put("callbackUri", callBackUri);//"");
		doc.put("authMode", 1);
		doc.put("signPositionMode", "0");
		doc.put("offsetX", x);
		doc.put("offsetY", y);
		doc.put("pagePosition", page);
		doc.put("enableTimestamp", true);
		doc.put("signatureMode", "1");
		doc.put("metaData", metaData);

		final String body = doc.toString();
		final String sha256 = sha256Base64(body);
		final String signature = createSignatureHasUid(SS_API_SECRET_KEY, uid, "POST", sha256, JSON_CONTENT_TYPE, date,
				"", SIGN_PDF, null);
		// Headers
		HashMap<String, String> headerMap = new HashMap<>();
		headerMap.put(HEADER_CONTENT_TYPE, JSON_CONTENT_TYPE);
		headerMap.put(HEADER_X_DATE, date);
		headerMap.put(HEADER_X_APP_ID, SS_APP_ID);
		headerMap.put(HEADER_X_UID, uid);
		headerMap.put(HEADER_X_SIGNATURE, signature);
		headerMap.put(HEADER_X_VERSION, SS_API_VERSION);
		// Url
		final String urlText = BASE_DOC_URL.concat(SIGN_PDF);
		return postData(urlText, headerMap, body);
	}

	static String fetchPendingDocuments(final String email) throws Exception {
		final String uid = getSignUid(email, SS_API_SECRET_KEY);
		final String date = getCurrentDateTimeGMT();
		final String signature = createSignatureHasUid(SS_API_SECRET_KEY, uid, "GET", "", JSON_CONTENT_TYPE, date, "",
				PENDING_DOCUMENTS, null);
		// Headers
		HashMap<String, String> headerMap = new HashMap<>();
		headerMap.put(HEADER_CONTENT_TYPE, JSON_CONTENT_TYPE);
		headerMap.put(HEADER_X_DATE, date);
		headerMap.put(HEADER_X_APP_ID, SS_APP_ID);
		headerMap.put(HEADER_X_UID, uid);
		headerMap.put(HEADER_X_SIGNATURE, signature);
		headerMap.put(HEADER_X_VERSION, SS_API_VERSION);
		// Path
		final String encryptedEmail = encryptToHex(email, SS_API_SECRET_KEY);
		// Url
		final String urlText = BASE_DOC_URL.concat(PENDING_DOCUMENTS).concat("/").concat(encryptedEmail);
		return getData(urlText, headerMap);
	}

	static String postFCMToken(final String email, final String fcmToken) throws Exception {
		final String uid = getSignUid(email, SS_API_SECRET_KEY);
		final String date = getCurrentDateTimeGMT();
		final String signature = createSignatureHasUid(SS_API_SECRET_KEY, uid, "POST", "", JSON_CONTENT_TYPE, date, "",
				DEVICE_REGISTER, null);
		// Headers
		HashMap<String, String> headerMap = new HashMap<>();
		headerMap.put(HEADER_CONTENT_TYPE, JSON_CONTENT_TYPE);
		headerMap.put(HEADER_X_DATE, date);
		headerMap.put(HEADER_X_APP_ID, SS_APP_ID);
		headerMap.put(HEADER_X_UID, uid);
		headerMap.put(HEADER_X_SIGNATURE, signature);
		// Body
		JSONObject deviceRegisterParams = new JSONObject();
		deviceRegisterParams.put("cidKeyOwner", email);
		deviceRegisterParams.put("token", fcmToken);
		final String body = deviceRegisterParams.toString();
		// Urls
		final String urlText = BASE_DOC_URL.concat(DEVICE_REGISTER);
		final String result = postData(urlText, headerMap, body);
		return result;
	}

	// Functions
	public static String getData(final String urlText, final Map<String, String> headerMap) throws IOException {
		final URL url = new URL(urlText);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		if (headerMap != null) {
			for (String key : headerMap.keySet())
				urlConnection.setRequestProperty(key, headerMap.get(key));
		}

		urlConnection.setReadTimeout(20000 /* milliseconds */);
		urlConnection.setConnectTimeout(20000 /* milliseconds */);
		urlConnection.connect();

		int responseCode = urlConnection.getResponseCode();
		if (!isSuccessCode(responseCode)) {
			String errorMsg = "Unknown, response code=" + responseCode;
			InputStream error = urlConnection.getErrorStream();
			if (error != null) {
				errorMsg = readIn(error);
				errorMsg = errorMsg + ", response code=" + responseCode;
			}
			throw new IOException(errorMsg);
		} else {
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			String s = readIn(in);
			return s;
		}
	}

	public static String postData(final String urlText, final HashMap<String, String> headerMap, final String body)
			throws IOException {
		final URL url = new URL(urlText);
		HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		if (headerMap != null) {
			for (String key : headerMap.keySet())
				urlConnection.setRequestProperty(key, headerMap.get(key));
		}

		urlConnection.setDoOutput(true);
		urlConnection.setDoInput(true);
		urlConnection.setReadTimeout(20000 /* milliseconds */);
		urlConnection.setConnectTimeout(20000 /* milliseconds */);
		urlConnection.setRequestMethod("POST");
		urlConnection.connect();
		if (body != null && body.length() > 0) {
			PrintWriter pw = new PrintWriter(urlConnection.getOutputStream());
			pw.print(body);
			pw.flush();
			pw.close();
		}

		int responseCode = urlConnection.getResponseCode();
		if (!isSuccessCode(responseCode)) {
			String errorMsg = "Unknown, response code=" + responseCode;
			InputStream error = urlConnection.getErrorStream();
			if (error != null) {
				errorMsg = readIn(error);
				errorMsg = errorMsg + ", response code=" + responseCode;
			}
			throw new IOException(errorMsg);
		} else {
			InputStream in = new BufferedInputStream(urlConnection.getInputStream());
			String s = readIn(in);
			return s;
		}
	}

	private static boolean isSuccessCode(int code) {
		return code >= 200 && code < 300;
	}

	private static String readIn(InputStream in) throws IOException {
		StringBuilder sb = new StringBuilder();
		BufferedReader r = new BufferedReader(new InputStreamReader(in), 2048);
		for (String line = r.readLine(); line != null; line = r.readLine()) {
			sb.append(line);
		}
		in.close();
		return sb.toString();
	}

	public static String getCurrentDateTimeGMT() {
		final String DATE_TIME_FORMAT_GMT = "E, dd MMM yyyy HH:mm:ss 'GMT'";
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat(DATE_TIME_FORMAT_GMT, Locale.US);
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatGmt.format(new Date());
	}

	public static String getSignUid(String username, String appKey) throws Exception {
		String uid = encrypt(username, appKey);
		if (uid != null && uid.endsWith("\n")) {
			uid = uid.substring(0, uid.length() - 1);
		}
		uid = uid.replaceAll("(\n)", "");
		return uid;
	}

	public static String createSignatureHasUid(String appKey, String username, String httpMethod, String contentMD5,
			String contentType, String date, String range, String canonicalizedResource,
			Map<String, String> canonicalizedCustomizedHeadersWithoutUid) {
		Map<String, String> headers = new HashMap<String, String>();
		String uid = username.toLowerCase();
		headers.put("X-UID", uid);

		if (canonicalizedCustomizedHeadersWithoutUid != null && canonicalizedCustomizedHeadersWithoutUid.size() > 0)
			headers.putAll(canonicalizedCustomizedHeadersWithoutUid);

		return createSignature(appKey, username, httpMethod, contentMD5, contentType, date, range,
				canonicalizedResource, headers);
	}

	public static String createSignature(String appKey, String username, String httpMethod, String contentMD5,
			String contentType, String date, String range, String canonicalizedResource,
			Map<String, String> canonicalizedCustomizedHeaders) {
		StringBuilder headers = new StringBuilder();
		if (canonicalizedCustomizedHeaders != null && canonicalizedCustomizedHeaders.size() > 0) {
			List<String> keys = new ArrayList<String>();
			keys.addAll(canonicalizedCustomizedHeaders.keySet());
			Collections.sort(keys);
			final int size = keys.size();
			for (int idx = 0; idx < size; idx++) {
				headers.append(keys.get(idx).trim().toLowerCase());
				headers.append(":");
				headers.append(canonicalizedCustomizedHeaders.get(keys.get(idx).trim()));
				if (idx < size - 1) {
					headers.append("\n");
				}
			}
		}

		String strToSign = httpMethod + "\n" + contentMD5 + "\n" + contentType + "\n" + date + "\n" + range + "\n"
				+ canonicalizedResource.toLowerCase() + "\n" + headers.toString();

		String signature = encodeHmacSHA256(appKey, strToSign);
		if (signature != null && signature.endsWith("\n")) {
			signature = signature.substring(0, signature.length() - 1);
		}

		return signature;
	}

	private static String encodeHmacSHA256(String secret, String message) {
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			byte[] data = sha256_HMAC.doFinal(message.getBytes());
			String hash = Base64.getEncoder().encodeToString(data);
			return hash;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	private static String encrypt(String plainText, String key) throws Exception {
		byte[] plainTextbytes = plainText.getBytes(CHARACTER_ENCODING);
		byte[] keyBytes = getKeyBytes(key, AES256_KEY_SIZE);
		byte[] initVector = new byte[IV_SIZE];
		for (int i = 0; i < initVector.length; i++) {
			initVector[i] = 0;
		}
		return Base64.getEncoder().encodeToString(encrypt(plainTextbytes, keyBytes, initVector)).trim();
	}

	private static byte[] getKeyBytes(String key, int keySize) throws UnsupportedEncodingException {
		byte[] keyBytes = new byte[keySize];
		for (int i = 0; i < keyBytes.length; i++) {
			keyBytes[i] = 0;
		}
		byte[] parameterKeyBytes = key.getBytes(CHARACTER_ENCODING);
		System.arraycopy(parameterKeyBytes, 0, keyBytes, 0, Math.min(parameterKeyBytes.length, keyBytes.length));
		return keyBytes;
	}

	private static byte[] encrypt(byte[] plainText, byte[] key, byte[] initialVector)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
		Cipher cipher = Cipher.getInstance(CIPHER_TRANSFORMATION);
		SecretKeySpec secretKeySpec = new SecretKeySpec(key, AES_ALGORITHM);
		IvParameterSpec ivParameterSpec = new IvParameterSpec(initialVector);
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, ivParameterSpec);
		plainText = cipher.doFinal(plainText);
		return plainText;
	}

	private static String encryptToHex(String plainText, String key) throws Exception {
		byte[] plainTextbytes = plainText.getBytes(CHARACTER_ENCODING);
		byte[] keyBytes = getKeyBytes(key, AES256_KEY_SIZE);
		byte[] initVector = new byte[IV_SIZE];
		for (int i = 0; i < initVector.length; i++) {
			initVector[i] = 0;
		}

		return byteArrayToHexString(encrypt(plainTextbytes, keyBytes, initVector)).trim();
	}

	private static String byteArrayToHexString(byte[] buf) {
		char[] chars = new char[2 * buf.length];
		for (int i = 0; i < buf.length; ++i) {
			chars[2 * i] = HEX_CHARS[(buf[i] & 0xF0) >>> 4];
			chars[2 * i + 1] = HEX_CHARS[buf[i] & 0x0F];
		}
		return new String(chars);
	}

	private static String sha256Base64(String input) throws NoSuchAlgorithmException {
		MessageDigest mDigest = MessageDigest.getInstance("SHA-256");
		byte[] result = mDigest.digest(input.getBytes());
		return Base64.getEncoder().encodeToString(result);
	}
}
