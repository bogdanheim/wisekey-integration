package ch.aurachain.integration.sign.controller;

import ch.aurachain.integration.sign.document.client.ApiClient;
import ch.aurachain.integration.sign.jsondb.JsonDBManager;
import ch.aurachain.integration.sign.jsondb.model.SignDocument;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@RestController
@RequestMapping(value = "/doc")
public class DocumentController {

//    @PostMapping("/host/{pass}")
//    public ResponseEntity setHost(@PathVariable("pass") String pass, @RequestBody String host) {
//        if("superpass".equals(pass)) {
//            ApiClient.setHost(host);
//            return ResponseEntity.ok(null);
//        }
//        return ResponseEntity.badRequest().body("Wrong pass!");
//    }
//
//    @GetMapping("/host")
//    public ResponseEntity getHost() {
//        return ResponseEntity.ok(ApiClient.getHost());
//    }

    @GetMapping("/download/{docID}")
    public ResponseEntity<ByteArrayResource> downloadDocument(@PathVariable("docID") String docID) throws IOException {

        SignDocument signDoc = JsonDBManager.findById(docID, SignDocument.class);

        File doc = new File("./files/" + signDoc.getFileUUID() + "-" + signDoc.getFileName());
        byte[] docContent = Files.readAllBytes(doc.toPath());
        signDoc.setStatus("pending");
        JsonDBManager.save(signDoc);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + doc.getName())
                .contentType(MediaType.APPLICATION_PDF)
                .body(new ByteArrayResource(docContent));
    }
}
