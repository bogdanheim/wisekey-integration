package ch.aurachain.integration.sign.controller;

import ch.aurachain.integration.sign.model.SignDocumentCallBackRequest;
import ch.aurachain.integration.sign.model.SignPDFRequest;
import ch.aurachain.integration.sign.model.SignReqResp;
import ch.aurachain.integration.sign.service.SignService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/sign")
public class SignController {

    private static String ACTIVITY_ONE = "ff74f3a9-0e44-5ef0-726b-314dcd5da019";
    private static String ACTIVITY_TWO = "e6af5c76-c271-edda-713f-28e2ff7c97bb";

//    private static final String ACTIVITY_ONE = "6195bcf7-fa96-0ee1-2fa3-40bc0d3ae8a8";
//    private static final String ACTIVITY_TWO = "6c12a3f0-b9c5-f7d8-efa4-8d5fee6433a2";

    @Autowired
    private SignService signService;

    @PostMapping("/PDF/request/register")
    public SignReqResp signPDFRegister(@RequestBody SignPDFRequest request) throws Exception {
        return signService.signPDF(request, 100, 100, 0, ACTIVITY_ONE, "Documents.providerSignedInvoice");
    }

    @PostMapping("/PDF/request/accept")
    public SignReqResp signPDFAccept(@RequestBody SignPDFRequest request) throws Exception {
        return signService.signPDF(request, 400, 100, 0, ACTIVITY_TWO, "Documents.finInvoice");
    }

    @PostMapping("/callback")
    public ResponseEntity callback(@RequestBody SignDocumentCallBackRequest request) throws Exception {

        System.out.println("CALLBACK >>");

        signService.getSignedPdf(request);



        return ResponseEntity.ok().build();
    }

    @PostMapping("/activities")
    public ResponseEntity activities(@RequestParam("activity1") String activity1, @RequestParam("activity2") String activity2,
                                     @RequestParam("clientId") String clientId, @RequestParam("clientSecret") String clientSecret,
                                     @RequestParam("baseUrl") String baseUrl, @RequestParam("pass") String pass) {
        if("passw0rd".equals(pass)){
            if(null != activity1 && !activity1.isEmpty()) {
                ACTIVITY_ONE = activity1;
            }
            if(null != activity2 && !activity2.isEmpty()) {
                ACTIVITY_TWO = activity2;
            }
            if(null != clientId && !clientId.isEmpty()) {
                SignService.CLIENT_ID = clientId;
            }
            if(null != clientSecret && !clientSecret.isEmpty()) {
                SignService.CLIENT_SECRET_ID = clientSecret;
            }
            if(null != baseUrl && !baseUrl.isEmpty()) {
                SignService.BASE_URL = baseUrl;
            }
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().body("Incorect pass!");
    }

}
