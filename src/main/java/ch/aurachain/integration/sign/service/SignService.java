package ch.aurachain.integration.sign.service;

import ch.aurachain.integration.sign.document.client.api.AttachmentControllerApi;
import ch.aurachain.integration.sign.exposeapi.client.api.ProcessDefinitionControllerApi;
import ch.aurachain.integration.sign.jsondb.JsonDBManager;
import ch.aurachain.integration.sign.jsondb.model.SignDocument;
import ch.aurachain.integration.sign.model.SignDocumentCallBackRequest;
import ch.aurachain.integration.sign.model.SignPDFRequest;
import ch.aurachain.integration.sign.model.SignPDFResponse;
import ch.aurachain.integration.sign.model.SignReqResp;
import ch.aurachain.integration.sign.wisekey.WiseSign;
import ch.aurachain.integration.sign.wisekey.client.api.KeyApi;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.FileUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.util.Base64Utils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SignService {

    private File filesDir = new File("./files");
    private final AttachmentControllerApi attachApi = new AttachmentControllerApi();
    private final ProcessDefinitionControllerApi exposeApi = new ProcessDefinitionControllerApi();
    private final KeyApi keyApi = new KeyApi();


    private final String DOC_BASE_URI = "http://delivery-hl.aurachain.ch:9001/doc/download/";
    private final String CALL_BACK_BASE_URL = "http://delivery-hl.aurachain.ch:9001/sign/callback";

    private final String wise_user = "bogdan.heim@aurachain.ch";
//    private final String wise_user = "bogdan.heim@aurachain.ch";


    private static ObjectMapper MAPPER = new ObjectMapper();

    public static String CLIENT_ID = "3RiYncLZruHaDDIU0-qL2tMa8ys";
    public static String CLIENT_SECRET_ID = "OiUvCR6UOCyIjpLHVWiyyjezBvKmo5li18ymk0Y1r0U";
    public static String BASE_URL = "https://delivery.kb01.aurachain.ch/";

    private static final String TOKEN_URL = "integration/anonymous/oauth/authorization/token";
    private static final String AUTH_URL = "integration/anonymous/oauth/authorization/authorize";
    private static final String EXPOSE_API_URL = "integration/anonymous/bpmProcess/expose/api/";

    public SignReqResp signPDF(SignPDFRequest request, Integer x, Integer y, Integer page, String uploadActivity, String uploadKey) throws Exception {
        String fileB64 = request.getFileID();
        String appendName = "first";
        if(!"Documents.providerSignedInvoice".equals(uploadKey)){
            appendName = "second";
        }

        checkFileDir();
        File file = new File(filesDir.getAbsolutePath() + "/" + request.getInstance() + "-" + appendName + "-Invoice.pdf");
        byte[] bytes = Base64Utils.decodeFromUrlSafeString(fileB64);
        FileUtils.writeByteArrayToFile( file, bytes );

        SignDocument signDocument = buildSignDocument(request, appendName, "requested");

        String docUri = DOC_BASE_URI + signDocument.getId();

        Map<String, Object> metaData = buildMetaData(signDocument);

        System.out.println("Requester: " + request.getRequester());
        System.out.println("Signer: " + request.getCidKeyOwner());
        System.out.println("DOC_BASE_URI: " + docUri);
        System.out.println("CALL_BACK_BASE_URL: " + CALL_BACK_BASE_URL);
        System.out.println("metaData: " + metaData);
        JsonDBManager.update(signDocument);
        String response = WiseSign.signPdf(request.getRequester(), request.getCidKeyOwner(), docUri, CALL_BACK_BASE_URL,
                metaData, x, y, page);
        System.out.println(response);

        ObjectMapper mapper = new ObjectMapper();
        SignReqResp result = mapper.readValue(response, SignReqResp.class);

        signDocument.setSigningCode(result.getSigningCode());
        signDocument.setExpireTime(result.getExpireTime());
        signDocument.setTimestamp(result.getTimestamp());
        signDocument.setUploadActivity(uploadActivity);
        signDocument.setUploadKey(uploadKey);
        JsonDBManager.update(signDocument);
        return result;
    }

    private Map<String, Object> buildMetaData(SignDocument signDocument) {
        Map<String, Object> metaData = new HashMap<>();
        metaData.put("fileID",signDocument.getFileUUID());
        metaData.put("fileName",signDocument.getFileName());
        metaData.put("processInstID",signDocument.getProcessInstID());
        return metaData;
    }

    private SignDocument buildSignDocument(SignPDFRequest request, String append, String status) {
        SignDocument signDocument = new SignDocument();
        signDocument.setFileUUID(request.getInstance() + "-" + append);
        signDocument.setFileName("Invoice.pdf");
        signDocument.setProcessInstID(request.getInstance());
        signDocument.setId(request.getInstance() + "-" + append);
        signDocument.setSigner(request.getCidKeyOwner());
        signDocument.setRequester(request.getRequester());
        signDocument.setStatus(status);
        return signDocument;
    }

    private void checkFileDir() {
        if(!filesDir.exists()){
            filesDir.mkdirs();
        } else if(!filesDir.isDirectory()) {
            filesDir.delete();
            filesDir.mkdirs();
        }
    }


    public void getSignedPdf(SignDocumentCallBackRequest request) throws Exception {
        System.out.println(request);

        String jq = String.format("/.[signingCode='%s']",request.getSigningcode());
        List<SignDocument> docs = JsonDBManager.find(jq, SignDocument.class);
        SignDocument signDoc = docs.get(0);
        signDoc.setStatus("signed");

        String content = WiseSign.getSignedDocument(signDoc.getRequester(), signDoc.getSigningCode());
        SignPDFResponse pdfResponse = new ObjectMapper().readValue(content, SignPDFResponse.class);

        File signedFile = new File("./files/" + signDoc.getFileUUID() + "-signed-" + signDoc.getFileName());
        byte[] bytes = Base64.getDecoder().decode(pdfResponse.getDocSignedHash());
        FileUtils.writeByteArrayToFile(signedFile,bytes);

        System.out.println(signDoc);

        Map<String, Object> data = buildExposeApiData(signDoc.getUploadKey(), "signed-" + signDoc.getFileName(),pdfResponse.getDocSignedHash());

        String principal = "{\"tenantId\":\"d634b20d-128e-4a57-97cf-7b7b01aeb901\",\"tenantDomain\":\"default\",\"userId\":\"2c39c58f-b4a5-40a9-9826-9dce8b57a2fa\",\"userEmail\":\"admin@aurachain.ch\",\"language\":null,\"userFirstName\":\"\",\"userLastName\":\"Admin\",\"tokenExpiry\":\"2020-01-14T11:43:17.443739296\",\"permissions\":[\"f:ui:adm\",\"f\"]}";
//        String principal = "{\"tenantId\":\"d634b20d-128e-4a57-97cf-7b7b01aeb901\",\"tenantDomain\":\"default\",\"userId\":\"2c39c58f-b4a5-40a9-9826-9dce8b57a2fa\",\"userEmail\":\"admin@aurachain.ch\",\"language\":\"EN\",\"userFirstName\":\"Admin\",\"userLastName\":\"Admin\",\"tokenExpiry\":\"2020-01-14T10:52:33.931132102\",\"permissions\":[\"f:ui:adm\",\"f\",\"f:ui:pd:c\"]}";

//        exposeApi.exposeApiUsingPOST(signDoc.getUploadActivity(), data, signDoc.getProcessInstID(), principal);
        callExposeApi(signDoc.getUploadActivity(), signDoc.getProcessInstID(), data);

        JsonDBManager.save(signDoc);
    }

    private void callExposeApi(String activityID, String processInstaceId, Map<String, Object> data) throws IOException, URISyntaxException {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new FormHttpMessageConverter());
        restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
        UriComponentsBuilder firstUrl = UriComponentsBuilder.fromHttpUrl(BASE_URL + AUTH_URL)
                .queryParam("client_id", CLIENT_ID)
                .queryParam("response_type", "code")
                .queryParam("state", "state")
                .queryParam("redirect_uri", "http://test.com");
        ResponseEntity<String> response = restTemplate.getForEntity(firstUrl.toUriString(), String.class);
        if (response.getStatusCodeValue() == 302) {
            HttpHeaders httpHeaders = response.getHeaders();
            String redirectUrl = httpHeaders.getFirst("Location");
            String code = null;
            for (NameValuePair parm : URLEncodedUtils.parse(new URI(redirectUrl), StandardCharsets.UTF_8)) {
                if (parm.getName().equalsIgnoreCase("code")) {
                    code = parm.getValue();
                }
            }
            UriComponentsBuilder secondUrl = UriComponentsBuilder.fromHttpUrl(BASE_URL + TOKEN_URL)
                    .queryParam("client_id", CLIENT_ID)
                    .queryParam("response_type", "code")
                    .queryParam("state", "state")
                    .queryParam("grant_type", "authorization_code")
                    .queryParam("client_secret", CLIENT_SECRET_ID)
                    .queryParam("code", code)
                    .queryParam("redirect_uri", "http://test.com");
            HttpHeaders headers = new HttpHeaders();
            headers.set("Content-Type", "application/x-www-form-urlencoded");
            HttpEntity request = new HttpEntity(headers);
            //get token
            ResponseEntity<String> response2 = restTemplate.postForEntity(secondUrl.toUriString(),request, String.class);
            if (response2.getStatusCodeValue() == 200) {
                String responseJson = response2.getBody();
                String token = (String) MAPPER.readValue(responseJson, Map.class).get("access_token");
                HttpHeaders headers2 = new HttpHeaders();
                headers2.set("Content-Type", "application/json");
                headers2.set("authorization", "Bearer "+token);
                HttpEntity request2 = new HttpEntity(data,headers2);
                ResponseEntity<Object> response3 = restTemplate.postForEntity(
                        BASE_URL + EXPOSE_API_URL + activityID + "/" + processInstaceId,
                        request2, Object.class);
                System.out.println("Succes!\n"+response3.getBody());
            }
        }
    }

    private Map<String, Object> buildExposeApiData(String uploadKey, String fileName, String fileContent) {
        Map<String, Object> data = new HashMap<>();
        String[] keys = uploadKey.split("\\.");
        Map<String, Object> aux = data;
        for (int i=0; i<keys.length -1; i ++){
            String key = keys[i];
            aux.put(key, new HashMap<>());
            aux = (Map<String, Object>) aux.get(key);
        }
        String base64String = Base64.getEncoder().encodeToString(fileName.getBytes()) + "\n" + fileContent;
        aux.put(keys[keys.length-1],base64String);
        return data;
    }
}
