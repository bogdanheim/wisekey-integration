package ch.aurachain.integration.sign.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Configuration
@Import(FabricCorsConfiguration.class)
@EnableSwagger2
public class SwaggerConfiguration {

    public static Docket docket;

    @Bean
    public Docket api() {
        if (docket == null) {
            Set<String> protocols = new HashSet<>();
            protocols.add("http");
            ApiInfo info = new ApiInfo("Sign API", "Sign API", "1.0", "urn:tos",
                null, null, null,
                new ArrayList<>());
            docket = new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("ch.aurachain.integration.sign"))
                .paths(PathSelectors.any())
                .build().apiInfo(info).protocols(protocols);
        }
        return docket;
    }
}
