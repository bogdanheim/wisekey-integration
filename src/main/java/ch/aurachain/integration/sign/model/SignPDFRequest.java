package ch.aurachain.integration.sign.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SignPDFRequest {

    private String fileID;
    private String requester;
    private String cidKeyOwner;
    private String instance;

}
