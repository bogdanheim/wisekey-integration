package ch.aurachain.integration.sign.model;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SignDocumentCallBackRequest {

    private String endpoint;
    private String signingcode;
    private String status;
}
