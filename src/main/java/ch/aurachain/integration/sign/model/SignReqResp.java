package ch.aurachain.integration.sign.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SignReqResp {

    @JsonProperty("$id")
    private String id;
    private Integer code;
    private String message;
    private String signingCode;
    private String expireTime;
    private Long timestamp;

}
